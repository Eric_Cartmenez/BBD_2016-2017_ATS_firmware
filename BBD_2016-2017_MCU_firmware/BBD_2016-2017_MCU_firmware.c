/*
 * BBD_2016_2017_MCU_firmware.c
 *
 * Created: 2/10/2017 9:31:33 AM
 *  Author: Andrew
 */



 
#ifndef F_CPU
#define F_CPU 16000000UL                    // set the CPU clock
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
//#include <stdlib.h>

  
#define BAUD 9600                           // define baud
#define BAUDRATE ((F_CPU)/(BAUD*16UL)-1)    // set baudrate value for UBRR

#define BUFFER_SIZE  6					//define buffer size in bytes for UART receive buffer
  
// Global variables used both in ISR/Main are declared as "volatile"
volatile uint8_t BUFFER[BUFFER_SIZE];		//UART buffer
volatile uint8_t BUFFER_COUNTER = 0;		//Counter to numerate bytes in the buffer
volatile uint8_t BUFFER_FILLED;				//Buffer filled flag

volatile uint8_t OP_CODE[BUFFER_SIZE];		//Operation code variable where buffer contents will be saved


// function to initialize UART
void uart_init (void)
{
    UBRR0H = (BAUDRATE>>8);								// shift the register right by 8 bits
    UBRR0L = BAUDRATE;									// set baud rate
    UCSR0B|= (1<<TXEN0)|(1<<RXEN0);						// enable receiver and transmitter
    UCSR0C|= (1<<UMSEL01)|(1<<UCSZ00)|(1<<UCSZ01);		// 8bit data format
	UCSR0B|= (1<<RXCIE0);								//Enable USART Receive Complete Flag interrupt [interrupt is generated when there's unread data in receive buffer]
	sei();												//Enable global interrupts
}



// function to send data
void uart_transmit (uint8_t data)
{
    while (!( UCSR0A & (1<<UDRE0)));                // wait while register is free
    UDR0 = data;                                   // load data in the register
}


/* Interrupt executes every time new byte arrives to UART receive buffer.
Flag is automatically cleared after data from UDR0 is read. */

ISR(USART_RX_vect)								
{
	while ( !(UCSR0A & (1<<RXC0)) );			//wait while data is being received
	
	if (BUFFER_COUNTER == (BUFFER_SIZE-1))			//check is buffer counter reached buffer size
	{
	BUFFER[BUFFER_COUNTER] = UDR0;	
	BUFFER_COUNTER = 0;							//if buffer counter reached buffer size, reset buffer counter
	BUFFER_FILLED = 1;						//Inform main than buffer was filled
	}
	else
	{
	BUFFER[BUFFER_COUNTER] = UDR0;			//Read received data to buffer
	BUFFER_COUNTER = BUFFER_COUNTER + 1;
	//BYTE_RECEIVED_FLAG = 1;						//Inform main than byte was received
	}	
	#ifdef DEBUG								// DEBUG routine [maybe designed later]
		printf(...);
	#endif
}




int main(void)
{
	DDRB |= (1<<PINB5);
	uart_init();
	uint8_t i = 0;
	uint8_t k = 0;
	
	while(1)
	{
		if (BUFFER_FILLED==1)						//Check is UART receive buffer is full and start buffer saving routine
		{	
			for(i=0; i < BUFFER_SIZE;i++)
				{
					OP_CODE[i] = BUFFER[i];
					BUFFER[i] = 0;
				}
			
			
			for(k=0; k < BUFFER_SIZE; k++)
				{
					uart_transmit(OP_CODE[k]);
				}
			BUFFER_FILLED=0;					// Unflag buffer received flag
		}	
	}

}